<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter Redis Authentification
 *
 * A CodeIgniter library to make an user auth with Redis
 *
 * @package   CodeIgniter
 * @category  Libraries
 * @author    Dominik Kukacka
 *
 */

class Redauth {
	
	/**
	 * CI
	 *
	 * CodeIgniter instance
	 * @var 	object
	 */
  private $_ci;

	/**
	 * Constructor
	 */
	public function __construct() {
		
		log_message('debug', 'Redauth Class Initialized');

		$this->_ci =& get_instance();
    $this->_ci->load->library('redis');
    $this->_ci->load->helper('cookie');
		$this->_ci->load->config('redauth');

	}

  /**
  * deletes all users from the db 
  *
  * @return boolean always true
  * @access public
  */

  public function deleteAllUsers() {
    foreach($this->listUsers() as $u) {
      $this->_ci->redis->srem( $this->_ci->config->item('redauth_prefix') . 'users', $u );
    }

    return true;
  }


  /**
  * returns all users
  *
  * @return array all usernames
  * @access public
  */

  public function listUsers() { 
    $arr = array();
    if( $this->_ci->redis->scard( $this->_ci->config->item('redauth_prefix') . 'users') ) {
      $arr = $this->_ci->redis->smembers( $this->_ci->config->item('redauth_prefix')  . 'users' );
    }
   return $arr;
  }



  public function __call( $name, $arguments ) {

    $username = $this->_getUsername();

    //handler for getter and setter 
    //if username not specified take current logged in user 

    $key = strtolower(substr($name, 3));
    if(substr($name, 0, 3) === "get") {
      
      //get username
      if($key === 'username') {
        return $this->_getUsername();
      } else {

        //get all other keys
        if(isset($arguments[0])) {
          $username = $arguments[0];
        }
        return $this->_get( $key, $username );

      }

    } elseif(substr($name, 0, 3) === 'set') {
      
      //handle value
      if(isset($arguments[0])) {
        $value = $arguments[0];
      } else {
        return false;
      }

      //take current user if not specified
      if(isset($arguments[1])) {
        $username = $arguments[1];
      }

      return $this->_set( $key, $value, $username );  

    }

  }


  /**
  * gets an data entry for an user
  *
  * @param  string  what
  * @param  string  username (if not specified the cur. logged in user will be used)
  * @return boolean the data entry
  * @access public
  */
  
  public function _get($what, $username ) {
    return $this->_ci->redis->get( $this->_ci->config->item('redauth_prefix') . 'users::' . $username . '::' . $what );
  }

  /**
  * sets an data entry for an user
  *
  * @param  string  what
  * @param  string  username (if not specified the cur. logged in user will be used)
  * @param  string  value to set
  * @return boolean the data entry
  * @access public
  */
  
  public function _set($what, $value, $username ) {
    if(false !== strstr($value, " ")) {
      $value = '"' . $value . '"';
    }
    return $this->_ci->redis->command('set ' .  $this->_ci->config->item('redauth_prefix') . 'users::' . $username . '::' . $what . ' ' . $value );
  }

  /**
  * get username from cookie authtoken
  *
  * @return string username
  * @access protected
  */
  
  protected function _getUsername() {

    $cookie_authtoken = get_cookie($this->_ci->config->item('redauth_prefix') . 'authtoken');
    $username         = $this->_ci->redis->get($this->_ci->config->item('redauth_prefix') . 'auth::' . $cookie_authtoken);

    return $username;

  }

  /**
  * is the user logged in
  *
  * @return boolean true if logged in / fals eif not
  * @access public
  */
  
  public function isLoggedIn() {

    $cookie_authtoken = get_cookie($this->_ci->config->item('redauth_prefix') . 'authtoken');
    $username         = $this->_ci->redis->get($this->_ci->config->item('redauth_prefix') . 'auth::' . $cookie_authtoken);
    $redis_authtoken  = $this->_get('authtoken', $username);

    if( $cookie_authtoken === $redis_authtoken ) {
      return true;
    } else {
      return false;
    }

    //$this->_ci->redis->set( $this->prefix . 'users::' . $username . '::' . $what, $value );

  }
	
  /**
  * registers an user
  *
  * @param  string  username
  * @param  string  password
  * @return boolean true if success / false if not   
  * @access public
  */

  public function register($username, $password) {

    if(!isset($username) || !isset($password)) {
      return false;
    }

    if(1 === $this->_ci->redis->sadd( $this->_ci->config->item('redauth_prefix') . 'users', $username)) {

      $password = sha1($password);
       
      $this->_set('password', $password, $username);
      #$this->_set('email', "test@test.at", $username);
      log_message( 'debug', "registered the user '". $username ."' in the redis database"); 
      return true;

    }

    log_message( 'debug', "user '". $username ."' already in the redis database"); 
    return false;
  }


  /**
  * log in an user 
  *
  * @param  string  username
  * @param  string  password
  * @return boolean true id log in was successful / false when not
  * @access public
  */
  public function login($username, $password) {

    //username is in the user list
    if( 1 !== $this->_ci->redis->sismember( $this->_ci->config->item('redauth_prefix') . 'users', $username) ) {
      return false;  
    }  

    //password is correct
    $db_password = $this->_ci->redis->get( $this->_ci->config->item('redauth_prefix') . 'users::' . $username . '::password' );
    $post_password = sha1($password);
    if( $db_password === $post_password ) {

      //set cookie auth key
      $authtoken = sha1( mt_rand() . $username . $post_password . microtime()  );
      $cookie = array(
        'name'   => $this->_ci->config->item('redauth_prefix') . 'authtoken',
        'value'  => $authtoken,
        'expire' => $this->_ci->config->item('redauth_login_time')
      );
      set_cookie($cookie);
      
      //insert authtoken into redis
      $this->_set( 'authtoken', $authtoken, $username );
      $this->_ci->redis->set( $this->_ci->config->item('redauth_prefix') . 'auth::' . $authtoken, $username);
      $this->_ci->redis->expire( $this->_ci->config->item('redauth_prefix') . 'auth::' . $authtoken, $this->_ci->config->item('redauth_login_time'));
      return true; 
    } else {
      return false;
    }

  }

  /**
  * log out the user 
  *
  * @return boolean true id log out was successful / false when not
  * @access public
  */
  public function logout() {

    $cookie_authtoken = get_cookie($this->_ci->config->item('redauth_prefix') . 'authtoken');
    $this->_ci->redis->del($this->_ci->config->item('redauth_prefix') . 'auth::' . $cookie_authtoken);
    delete_cookie($this->_ci->config->item('redauth_prefix') . 'authtoken');

  }

  /**
  * checks if a username exists
  *
  * @param string username
  * @return boolean true if exists / false if not
  * @access public
  */
  public function userExists( $username ) {

    return ( 1 === $this->_ci->redis->sismember( $this->_ci->config->item('redauth_prefix') . 'users', $username) );

  }
}
