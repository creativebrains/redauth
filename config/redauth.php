<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Config for the Redauth library
 *
 * @see ../libraries/Redauth.php
 */

$config['redauth_prefix']     = null;
$config['redauth_login_time'] = 604800;
