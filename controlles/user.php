<?php 
if (! defined('BASEPATH')) exit('No direct script access');

class User extends MX_Controller {
	
	function __construct() {
		
		$this->load->library('redauth');
		$this->load->helper('form');
		$this->load->helper('url');
		
		#$this->redis->set('key', 'value');
    #echo $this->redis->get('key');		
		
	}

  public function loginbox() {
    $this->load->view("user/loginbox");
  }

  public function index() {
    if($this->redauth->isLoggedIn()) {
      echo "Hallo " . $this->redauth->getUsername() . ' <small><a href="' . base_url('user/logout') . '">logout</a></small>';
    } else {
      echo 'please <a href="user/login">login</a>';
    }
  }

  public function profile() {
    $this->redauth->setName('Dominik Kukacka'); 
    echo $this->redauth->getName(); 
  }

  public function view( $username = null ) {
    
    if( null !== $username ) {
      if($this->redauth->userExists( $username )) {
        echo "This is " . $username . "! &lt;" . $this->redauth->getEmail($username) . "&gt;";
      } else {
      }
    } else {
      $users = $this->redauth->listUsers();
      if(sizeof($users) > 0) {
        foreach( $users as $u) {
          echo $u . " " . $this->redauth->get('authtoken', $u);
        }
      } else {
        echo "no users found";
      }
    }

  }

  public function clear() {
    $this->redauth->deleteAllUsers();
  }

  public function login() {
    
    if($this->input->post("username") && $this->input->post("password")) {
      var_dump($this->redauth->login($this->input->post("username"), $this->input->post("password")));
    } else {
      $this->load->view("login");
    }

  }

  public function logout() {
  
    var_dump($this->redauth->logout());

  }

  public function register() {

    if($this->input->post("username") && $this->input->post("password")) {
      var_dump($this->redauth->register($this->input->post("username"), $this->input->post("password")));
    } else {
      $this->load->view("register");
    }

  }

}
